# Milenium XML to ANS Schema

## Installation

```bash
$ composer require lvilabs/milenium-xml-to-ans-schema
```

## Usage

```bash
$ php ./bin/milenium-to-ans {source-file} {destination-file} --config={config-file} 
```

Example

```bash
php ./bin/milenium-to-ans example/source/site.xml example/destination example/config.json
```
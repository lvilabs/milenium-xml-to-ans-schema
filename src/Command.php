<?php

namespace MileniumToANS;

use MileniumToANS\Exception\InvalidArgumentException;
use Symfony\Component\Console\Command\Command as BaseCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class Command
 *
 * @package MileniumToANS
 */
class Command extends BaseCommand
{

    const STATUS_CODE_OK = 0;

    const STATUS_CODE_ERROR = 2;

    /**
     * @var string $defaultName
     */
    protected static $defaultName = 'milenium-to-ans';

    /**
     * @var DataInterface $data
     */
    private $data;

    /**
     * Command constructor.
     *
     * @param DataInterface $data
     */
    public function __construct(DataInterface $data)
    {
        parent::__construct(self::$defaultName);
        $this->data = $data;
    }

    /**
     * Configure command
     */
    protected function configure()
    {
        $this
            ->setDescription('Export Milenium XML to ANS Schema JSON.')
            ->setHelp('This commands allows you to export Milenium XML to ANS Schema JSON.')
            ->addArgument('source', InputArgument::REQUIRED, 'Source file.')
            ->addArgument('destination', InputArgument::REQUIRED, 'Destination directory.')
            ->addOption('config', 'c', InputOption::VALUE_OPTIONAL, 'Config');
    }

    /**
     * Execute command
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $site = $this->data->gather($input->getArgument('source'));
            $config = $this->data->config($input->getOption('config'));
            $articles = $this->data->process($site, $config);
            $this->data->save($input->getArgument('destination'), $articles);
        } catch (InvalidArgumentException $exception) {
            $output->writeln($exception->getMessage());
            return self::STATUS_CODE_ERROR;
        }

        return self::STATUS_CODE_OK;
    }

}

<?php

namespace MileniumToANS;

use Symfony\Component\Filesystem\Filesystem as SymfonyFilesystem;

/**
 * Interface FilesystemInterface
 *
 * @package MileniumToANS
 */
class Filesystem implements FilesystemInterface
{

    /**
     * @var SymfonyFilesystem $filesystem
     */
    private $filesystem;

    /**
     * Filesystem constructor.
     */
    public function __construct()
    {
        $this->filesystem = new SymfonyFilesystem();
    }

    /**
     * {@inheritDoc}
     */
    public function fileExists(string $fileName): bool
    {
        return $this->filesystem->exists($fileName);
    }

    /**
     * {@inheritDoc}
     */
    public function createDirectory(string $directoryName): bool
    {
        try {
            $this->filesystem->mkdir($directoryName);
            $return = true;
        } catch (\Exception $exception) {
            $return = false;
        }
        return $return;
    }

    /**
     * {@inheritDoc}
     */
    public function createFile(string $fileName, string $content): bool
    {
        try {
            $this->filesystem->dumpFile($fileName, $content);
            $return = true;
        } catch (\Exception $exception) {
            $return = false;
        }
        return $return;
    }

    /**
     * {@inheritDoc}
     */
    public function readFileContents(string $fileName): string
    {
        return file_get_contents($fileName);
    }

}

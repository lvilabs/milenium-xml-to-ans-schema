<?php

namespace MileniumToANS;

use ANSSchema\Header;
use ANSSchema\Headlines;
use ANSSchema\Image;
use ANSSchema\Planning;
use ANSSchema\PromoItems;
use ANSSchema\Section;
use ANSSchema\Story;
use ANSSchema\Subheadlines;
use ANSSchema\Taxonomy;
use ANSSchema\Text;
use ANSSchema\Workflow;
use Milenium\Element\Site;
use Milenium\Element\Section as MileniumSection;

/**
 * Class Processor
 *
 * @package MileniumToANS
 */
class Processor implements ProcessorInterface
{

    public function process(Site $site, array $config): array
    {
        $articles = [];

        foreach ($site->sections as $section) {
            $primarySection = $this->processSection($section, $config);

            foreach ($section->articles as $article) {
                $headlines = (new Headlines())
                    ->setBasic(
                        strip_tags($article->headline[0][0]['content'] ?? '')
                    );

                $subheadlines = (new Subheadlines())
                    ->setBasic(
                        strip_tags($article->subheadline[0][0]['content'] ?? '')
                    );

                $contentElements = [];

                foreach ($article->body as $bodyElements) {
                    foreach ($bodyElements as $bodyElement) {
                        ['type' => $type, 'content' => $content] = $bodyElement;

                        switch ($type) {
                            case 'header':
                                $element = (new Header())
                                    ->setType(Header::HEADER)
                                    ->setLevel(3)
                                    ->setContent($content);
                                break;
                            default:
                                $element = (new Text())
                                    ->setType(Text::TEXT)
                                    ->setContent($content);
                                break;
                        }

                        $contentElements[] = $element;
                    }
                }

                $planning = (new Planning())
                    ->setInternalNote('Migrated from Milenium');

                $workflow = (new Workflow())
                    ->setStatusCode(1);

                $story = (new Story())
                    ->setCreatedDate($article->date)
                    ->setLastUpdatedDate($article->date)
                    ->setType(Story::STORY)
                    ->setVersion(Story::CONST_0_10_4)
                    ->setHeadlines($headlines)
                    ->setSubheadlines($subheadlines)
                    ->setTaxonomy((new Taxonomy())
                        ->setPrimarySection($primarySection)
                    )
                    ->setWorkflow($workflow)
                    ->setPlanning($planning)
                    ->setContentElements($contentElements);

                if (!empty($article->image)) {
                    $image = (new Image())
                        ->setVersion(Image::CONST_0_10_4)
                        ->setType(Image::IMAGE)
                        ->setUrl($article->image->url);
                    $promoItems = (new PromoItems())
                        ->setBasic($image);

                    $story->setPromoItems($promoItems);
                }

                $articles[$article->id] = $story;
            }
        }

        return $articles;
    }

    private function processSection(MileniumSection $section, array $config)
    {
        if (!empty($config['sections'][$section->name])) {
            $section = $config['sections'][$section->name];
        } else {
            $section = [
                'name' => $section->name,
                'website' => '',
            ];
        }
        return (new Section())
            ->setVersion(Section::CONST_0_10_4)
            ->setType(Section::SECTION)
            ->setName($section['name'])
            ->setWebsite($section['website']);
    }

}

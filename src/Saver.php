<?php

namespace MileniumToANS;

use ANSSchema\Story;
use MileniumToANS\Exception\MissingDestinationFolderException;

class Saver implements SaverInterface
{

    private $filesystem;

    public function __construct(FilesystemInterface $filesystem)
    {
        $this->filesystem = $filesystem;
    }

    public function save(string $destination, array $articles): void
    {
        if (!$this->filesystem->createDirectory($destination)) {
            throw new MissingDestinationFolderException;
        }

        foreach ($articles as $index => $article) {
            $this->filesystem->createFile(
                $destination . '/' . $index . '.json',
                json_encode(Story::export($article))
            );
        }
    }

}

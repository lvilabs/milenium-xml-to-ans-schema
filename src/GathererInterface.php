<?php

namespace MileniumToANS;

use Milenium\Element\Site;
use MileniumToANS\Exception\InvalidArgumentException;

/**
 * Interface GathererInterface
 *
 * @package MileniumToANS
 */
interface GathererInterface
{

    /**
     * Gather data from source
     *
     * @param string $source
     *
     * @return Site
     *
     * @throws InvalidArgumentException
     */
    public function gather(string $source): Site;

}
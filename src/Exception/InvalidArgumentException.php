<?php

namespace MileniumToANS\Exception;

use InvalidArgumentException as BaseException;

/**
 * Class InvalidArgumentException
 *
 * @package MileniumToANS\Exception
 */
class InvalidArgumentException extends BaseException
{
}

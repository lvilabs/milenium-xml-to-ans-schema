<?php

namespace MileniumToANS\Exception;

/**
 * Class MissingDestinationFolderException
 *
 * @package MileniumToANS\Exception
 */
class MissingDestinationFolderException extends InvalidArgumentException
{

    /**
     * @var string $message
     */
    protected $message = 'Missing destination folder.';

}

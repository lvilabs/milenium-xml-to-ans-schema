<?php

namespace MileniumToANS\Exception;

/**
 * Class WrongConfigFormatException
 *
 * @package MileniumToANS\Exception
 */
class WrongConfigFormatException extends InvalidArgumentException
{

    /**
     * @var string $message
     */
    protected $message = 'Wrong config format. Expected JSON';

}

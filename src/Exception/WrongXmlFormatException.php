<?php

namespace MileniumToANS\Exception;

/**
 * Class WrongXmlFormatException
 *
 * @package MileniumToANS\Exception
 */
class WrongXmlFormatException extends InvalidArgumentException
{

    /**
     * @var string $message
     */
    protected $message = 'Wrong XML Format.';

}

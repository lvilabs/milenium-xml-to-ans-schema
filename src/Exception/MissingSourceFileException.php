<?php

namespace MileniumToANS\Exception;

/**
 * Class MissingSourceFileException
 *
 * @package MileniumToANS\Exception
 */
class MissingSourceFileException extends InvalidArgumentException
{

    /**
     * @var string $message
     */
    protected $message = 'Missing source file.';

}

<?php

namespace MileniumToANS\Exception;

/**
 * Class MissingConfigFileException
 *
 * @package MileniumToANS\Exception
 */
class MissingConfigFileException extends InvalidArgumentException
{

    /**
     * @var string $message
     */
    protected $message = 'Missing config file.';

}

<?php

namespace MileniumToANS;

/**
 * Interface DataInterface
 *
 * @package MileniumToANS
 */
interface DataInterface extends
    GathererInterface,
    ConfigInterface,
    ProcessorInterface,
    SaverInterface
{
}

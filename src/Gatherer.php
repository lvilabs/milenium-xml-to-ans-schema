<?php

namespace MileniumToANS;

use Milenium\Client;
use Milenium\Element\Article;
use Milenium\Element\Site;
use Milenium\Exception\InvalidArgumentException;
use MileniumToANS\Exception\MissingSourceFileException;
use MileniumToANS\Exception\WrongXmlFormatException;

/**
 * Class Gatherer
 *
 * @package MileniumToANS
 */
class Gatherer implements GathererInterface
{

    /**
     * @var FilesystemInterface $filesystem
     */
    protected $filesystem;

    /**
     * @var Client $siteCrawler
     */
    protected $siteCrawler;

    /**
     * @var Client $articleCrawler
     */
    protected $articleCrawler;

    /**
     * @var Client $imageCrawler
     */
    protected $imageCrawler;

    /**
     * @var string $dirname
     */
    protected $dirname = '';

    /**
     * Gatherer constructor.
     *
     * @param FilesystemInterface $filesystem
     * @param Client $siteCrawler
     * @param Client $articleCrawler
     * @param Client $imageCrawler
     */
    public function __construct(
        FilesystemInterface $filesystem,
        Client $siteCrawler,
        Client $articleCrawler,
        Client $imageCrawler
    )
    {
        $this->filesystem = $filesystem;
        $this->siteCrawler = $siteCrawler;
        $this->articleCrawler = $articleCrawler;
        $this->imageCrawler = $imageCrawler;
    }

    /**
     * {@inheritDoc}
     */
    public function gather(string $source): Site
    {
        if (!$this->filesystem->fileExists($source)) {
            throw new MissingSourceFileException();
        }

        $data = $this->filesystem->readFileContents($source);
        $this->dirname = dirname($source);

        return $this->getSite($data);
    }

    /**
     * Get site data from XML
     *
     * @param string $data
     *
     * @return Site
     */
    private function getSite(string $data): Site
    {
        try {
            $this->siteCrawler->fromString($data);
            $site = $this->siteCrawler->getData();
        } catch (InvalidArgumentException $exception) {
            throw new WrongXmlFormatException();
        }

        foreach ($site->sections as &$section) {
            foreach ($section->articles as &$article) {
                $this->getArticle($article);
                unset($article);
            }
            unset($section);
        }

        return $site;
    }

    /**
     * Get article from XML
     *
     * @param Article $article
     */
    private function getArticle(Article &$article)
    {
        $articleFile = $this->dirname . '/' . $article->xml;
        if ($this->filesystem->fileExists($articleFile)) {
            $articleContent = $this->filesystem->readFileContents($articleFile);
            try {
                $this->articleCrawler->fromString($articleContent);
                $article->update((array)$this->articleCrawler->getData());

                if (!empty($article->image)) {
                    $this->getImage($article);
                }
            } catch (InvalidArgumentException $exception) {
            }
        }
    }

    /**
     * Get image from reference
     *
     * @param Article $article
     *
     * @throws InvalidArgumentException
     */
    private function getImage(Article &$article)
    {
        $imageFile = $this->dirname . '/' . $article->image->xml;
        if ($this->filesystem->fileExists($imageFile)) {
            $imageContent = $this->filesystem->readFileContents($imageFile);
            $this->imageCrawler->fromString($imageContent);
            $article->image->update(
                (array)$this->imageCrawler->getData()
            );
        }
    }

}

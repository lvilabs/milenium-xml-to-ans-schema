<?php

namespace MileniumToANS;

use Milenium\Element\Site;

/**
 * Class Data
 *
 * @package MileniumToANS
 */
class Data implements DataInterface
{

    /**
     * @var GathererInterface $gatherer
     */
    protected $gatherer;

    /**
     * @var ProcessorInterface $processor
     */
    protected $processor;

    /**
     * @var SaverInterface $saver
     */
    protected $saver;

    /**
     * @var ConfigInterface $saver
     */
    protected $config;

    /**
     * Data constructor.
     *
     * @param GathererInterface $gatherer
     * @param ProcessorInterface $processor
     * @param SaverInterface $saver
     * @param ConfigInterface $config
     */
    public function __construct(
        GathererInterface $gatherer,
        ProcessorInterface $processor,
        SaverInterface $saver,
        ConfigInterface $config
    )
    {
        $this->gatherer = $gatherer;
        $this->processor = $processor;
        $this->saver = $saver;
        $this->config = $config;
    }

    /**
     * {@inheritDoc}
     */
    public function gather(string $source): Site
    {
        return $this->gatherer->gather($source);
    }

    /**
     * {@inheritDoc}
     */
    public function process(Site $site, array $config): array
    {
        return $this->processor->process($site, $config);
    }

    /**
     * {@inheritDoc}
     */
    public function save(string $destination, array $articles): void
    {
        $this->saver->save($destination, $articles);
    }

    /**
     * {@inheritDoc}
     */
    public function config(string $source = null): array
    {
        return $this->config->config($source);
    }

}

<?php

namespace MileniumToANS;

/**
 * Interface FilesystemInterface
 *
 * @package MileniumToANS
 */
interface FilesystemInterface
{

    /**
     * Check if string is a file name
     *
     * @param string $fileName
     *
     * @return bool
     */
    public function fileExists(string $fileName): bool;

    /**
     * Create a directory from string
     *
     * @param string $directoryName
     *
     * @return bool
     */
    public function createDirectory(string $directoryName): bool;

    /**
     * Create a file with contents
     *
     * @param string $fileName
     * @param string $content
     *
     * @return bool
     */
    public function createFile(string $fileName, string $content): bool;

    /**
     * Read file contents
     *
     * @param string $fileName
     *
     * @return string
     */
    public function readFileContents(string $fileName): string;

}

<?php

namespace MileniumToANS;

/**
 * Interface SaverInterface
 *
 * @package MileniumToANS
 */
interface SaverInterface
{

    /**
     * Save data
     *
     * @param string $destination
     * @param array $articles
     */
    public function save(string $destination, array $articles): void;

}
<?php

namespace MileniumToANS;

use MileniumToANS\Exception\InvalidArgumentException;

/**
 * Interface ConfigInterface
 *
 * @package MileniumToANS
 */
interface ConfigInterface
{

    /**
     * Get config from file
     *
     * @param string|null $source
     *
     * @return array
     *
     * @throws InvalidArgumentException
     */
    public function config(string $source = null): array;

}

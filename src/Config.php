<?php

namespace MileniumToANS;

use MileniumToANS\Exception\MissingConfigFileException;
use MileniumToANS\Exception\WrongConfigFormatException;

/**
 * Class Config
 *
 * @package MileniumToANS
 */
class Config implements ConfigInterface
{

    /**
     * @var FilesystemInterface $filesystem
     */
    private $filesystem;

    /**
     * Config constructor.
     *
     * @param FilesystemInterface $filesystem
     */
    public function __construct(FilesystemInterface $filesystem)
    {
        $this->filesystem = $filesystem;
    }

    /**
     * {@inheritDoc}
     */
    public function config(string $source = null): array
    {
        $config = [];

        if (!empty($source)) {
            if (!$this->filesystem->fileExists($source)) {
                throw new MissingConfigFileException();
            }

            $config = json_decode(
                $this->filesystem->readFileContents($source),
                true
            );

            if (empty($config)) {
                throw new WrongConfigFormatException;
            }
        }

        return $config;
    }

}

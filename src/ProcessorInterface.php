<?php

namespace MileniumToANS;

use Milenium\Element\Site;
use MileniumToANS\Exception\InvalidArgumentException;

/**
 * Interface ProcessorInterface
 *
 * @package MileniumToANS
 */
interface ProcessorInterface
{

    /**
     * Process data
     *
     * @param Site $site
     * @param array $config
     *
     * @return array
     *
     * @throws InvalidArgumentException
     */
    public function process(Site $site, array $config): array;

}
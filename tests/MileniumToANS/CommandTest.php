<?php

namespace Test\MileniumToANS;

use Milenium\Element\Site;
use MileniumToANS\Command;
use MileniumToANS\DataInterface;
use MileniumToANS\Exception\InvalidArgumentException;
use MileniumToANS\Exception\MissingConfigFileException;
use MileniumToANS\Exception\MissingDestinationFolderException;
use MileniumToANS\Exception\MissingSourceFileException;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Tester\CommandTester;

class CommandTest extends TestCase
{

    /**
     * @var DataInterface|MockObject $data
     */
    private $data;

    /**
     * @var CommandTester $commandTester
     */
    private $commandTester;

    protected function setUp(): void
    {
        $this->data = $this->getMockBuilder(DataInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        $application = new Application();
        $application->add(new Command($this->data));
        $command = $application->find('milenium-to-ans');
        $this->commandTester = new CommandTester($command);
    }

    protected function tearDown(): void
    {
        $this->commandTester = null;
    }

    public function testDataGatherThrowsException()
    {
        $source = 'missing-source-file';
        $destination = 'destination-folder';
        $config = 'config-file';

        $this->data->expects($this->once())
            ->method('gather')
            ->with($source)
            ->willThrowException(new MissingSourceFileException());

        $this->data->expects($this->never())
            ->method('config');

        $this->data->expects($this->never())
            ->method('process');

        $this->data->expects($this->never())
            ->method('save');

        $this->commandTester
            ->execute(
                [
                    'source' => $source,
                    'destination' => $destination,
                    '--config' => $config,
                ],
                ['verbosity' => OutputInterface::VERBOSITY_VERBOSE]
            );
        $this->assertStringContainsString('Missing source file.', $this->commandTester->getDisplay());
        $this->assertEquals(Command::STATUS_CODE_ERROR, $this->commandTester->getStatusCode());
    }

    public function testDataConfigThrowsException()
    {
        $source = 'source-file';
        $config = 'missing-config-file';
        $destination = 'destination-folder';
        $site = new Site();

        $this->data->expects($this->once())
            ->method('gather')
            ->with($source)
            ->willReturn($site);

        $this->data->expects($this->once())
            ->method('config')
            ->with($config)
            ->willThrowException(new MissingConfigFileException());

        $this->data->expects($this->never())
            ->method('process');

        $this->data->expects($this->never())
            ->method('save');

        $this->commandTester
            ->execute(
                [
                    'source' => $source,
                    'destination' => $destination,
                    '--config' => $config,
                ],
                ['verbosity' => OutputInterface::VERBOSITY_VERBOSE]
            );
        $this->assertEquals(Command::STATUS_CODE_ERROR, $this->commandTester->getStatusCode());
    }

    public function testDataProcessThrowsException()
    {
        $source = 'source-file';
        $config = 'config-file';
        $destination = 'destination-folder';
        $site = new Site();

        $this->data->expects($this->once())
            ->method('gather')
            ->with($source)
            ->willReturn($site);

        $this->data->expects($this->once())
            ->method('config')
            ->with($config)
            ->willReturn([]);

        $this->data->expects($this->once())
            ->method('process')
            ->with($site, [])
            ->willThrowException(new InvalidArgumentException());

        $this->data->expects($this->never())
            ->method('save');

        $this->commandTester
            ->execute(
                [
                    'source' => $source,
                    'destination' => $destination,
                    '--config' => $config,
                ],
                ['verbosity' => OutputInterface::VERBOSITY_VERBOSE]
            );
        $this->assertEquals(Command::STATUS_CODE_ERROR, $this->commandTester->getStatusCode());
    }

    public function testDataSaveThrowsException()
    {
        $source = 'source-file';
        $config = 'config-file';
        $destination = 'missing-destination-folder';
        $site = new Site();

        $this->data->expects($this->once())
            ->method('gather')
            ->with($source)
            ->willReturn($site);

        $this->data->expects($this->once())
            ->method('config')
            ->with($config)
            ->willReturn([]);

        $this->data->expects($this->once())
            ->method('process')
            ->with($site, [])
            ->willReturn([]);

        $this->data->expects($this->once())
            ->method('save')
            ->with($destination, [])
            ->willThrowException(new MissingDestinationFolderException);

        $this->commandTester
            ->execute(
                [
                    'source' => $source,
                    'destination' => $destination,
                    '--config' => $config,
                ],
                ['verbosity' => OutputInterface::VERBOSITY_VERBOSE]
            );
        $this->assertStringContainsString('Missing destination folder.', $this->commandTester->getDisplay());
        $this->assertEquals(Command::STATUS_CODE_ERROR, $this->commandTester->getStatusCode());
    }

    public function testExecuteOk()
    {
        $source = 'source-file';
        $destination = 'destination-folder';
        $site = new Site();

        $this->data->expects($this->once())
            ->method('gather')
            ->with($source)
            ->willReturn($site);

        $this->data->expects($this->once())
            ->method('process')
            ->with($site)
            ->willReturn([]);

        $this->data->expects($this->once())
            ->method('save');

        $this->commandTester
            ->execute(compact('source', 'destination'), ['verbosity' => OutputInterface::VERBOSITY_VERBOSE]);
        $this->assertEquals(Command::STATUS_CODE_OK, $this->commandTester->getStatusCode());
    }

}

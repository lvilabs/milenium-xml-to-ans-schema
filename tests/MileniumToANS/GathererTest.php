<?php

namespace Test\MileniumToANS;

use Milenium\Client;
use Milenium\Crawler\ArticleDomCrawler;
use Milenium\Crawler\DomCrawler;
use Milenium\Crawler\ImageDomCrawler;
use Milenium\Element\Site;
use MileniumToANS\Exception\MissingSourceFileException;
use MileniumToANS\Exception\WrongXmlFormatException;
use MileniumToANS\FilesystemInterface;
use MileniumToANS\Gatherer;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

/**
 * Class GathererTest
 *
 * @package MileniumToANS
 */
class GathererTest extends TestCase
{

    /**
     * @var FilesystemInterface|MockObject $filesystem
     */
    private $filesystem;

    /**
     * @var Gatherer $gatherer
     */
    private $gatherer;

    protected function setUp(): void
    {
        $this->filesystem = $this->getMockBuilder(FilesystemInterface::class)
            ->getMock();

        $this->gatherer = new Gatherer(
            $this->filesystem,
            new Client(new DomCrawler),
            new Client(new ArticleDomCrawler),
            new Client(new ImageDomCrawler)
        );
    }

    public function testGatherMissingSourceFile()
    {
        $source = 'missing-source-file';

        $this->expectException(MissingSourceFileException::class);

        $this->filesystem->expects($this->once())
            ->method('fileExists')
            ->with($source)
            ->willReturn(false);

        $this->gatherer->gather($source);
    }

    public function testGatherWrongXmlFormat()
    {
        $source = 'source-file';

        $this->expectException(WrongXmlFormatException::class);

        $this->filesystem->expects($this->once())
            ->method('fileExists')
            ->with($source)
            ->willReturn(true);

        $this->filesystem->expects($this->once())
            ->method('readFileContents')
            ->with($source)
            ->willReturn('<wrong-xml-format></wrong-xml-format>');

        $this->gatherer->gather($source);
    }

    public function testGatherOk()
    {
        $source = 'source-file';

        $this->filesystem->expects($this->exactly(3))
            ->method('fileExists')
            ->withConsecutive(
                [$source],
                ['./article.xml'],
                ['./image.xml']
            )
            ->willReturn(true);

        $siteData = file_get_contents(__DIR__ . '/../../example/source/site.xml');
        $articleData = file_get_contents(__DIR__ . '/../../example/source/article.xml');
        $imageData = file_get_contents(__DIR__ . '/../../example/source/image.xml');
        $this->filesystem->expects($this->exactly(3))
            ->method('readFileContents')
            ->withConsecutive(
                [$source],
                ['./article.xml'],
                ['./image.xml']
            )
            ->willReturn(
                $siteData,
                $articleData,
                $imageData
            );

        $site = $this->gatherer->gather($source);
        $this->assertInstanceOf(Site::class, $site);
        $this->assertEquals('Test', $site->name);
        $this->assertEquals('<I>Lorem ipsum dolor sit amet, consectetur adipiscing posuere.</I>', $site->sections[0]->articles[0]->headline[0][0]['content']);
        $this->assertEquals('data\image.jpg', $site->sections[0]->articles[0]->image->url);
    }

}

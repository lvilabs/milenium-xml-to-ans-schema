<?php

namespace Test\MileniumToANS;

use Milenium\Element\Site;
use MileniumToANS\ConfigInterface;
use MileniumToANS\Data;
use MileniumToANS\GathererInterface;
use MileniumToANS\ProcessorInterface;
use MileniumToANS\SaverInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class DataTest extends TestCase
{

    /**
     * @var Data $data
     */
    private $data;

    /**
     * @var GathererInterface|MockObject $gatherer
     */
    private $gatherer;

    /**
     * @var ProcessorInterface|MockObject $processor
     */
    private $processor;

    /**
     * @var SaverInterface|MockObject $saver
     */
    private $saver;

    /**
     * @var ConfigInterface|MockObject $saver
     */
    private $config;

    protected function setUp(): void
    {
        $this->gatherer = $this->getMockBuilder(GathererInterface::class)
            ->getMock();

        $this->processor = $this->getMockBuilder(ProcessorInterface::class)
            ->getMock();

        $this->saver = $this->getMockBuilder(SaverInterface::class)
            ->getMock();

        $this->config = $this->getMockBuilder(ConfigInterface::class)
            ->getMock();

        $this->data = new Data($this->gatherer, $this->processor, $this->saver, $this->config);
    }

    protected function tearDown(): void
    {
        $this->processor = null;
    }

    public function testGather()
    {
        $source = 'source-file';

        $this->gatherer->expects($this->once())
            ->method('gather')
            ->with($source)
            ->willReturn(new Site());

        $this->data->gather($source);
    }

    public function testProcess()
    {
        $site = new Site();

        $this->processor->expects($this->once())
            ->method('process')
            ->with($site, [])
            ->willReturn([]);

        $this->data->process($site, []);
    }

    public function testSave()
    {
        $this->saver->expects($this->once())
            ->method('save')
            ->with('destination-folder', []);

        $this->data->save('destination-folder', []);
    }

    public function testConfig()
    {
        $source = 'source-file';

        $this->config->expects($this->once())
            ->method('config')
            ->with($source)
            ->willReturn([]);

        $this->data->config($source);
    }

}

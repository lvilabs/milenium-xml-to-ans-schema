<?php

namespace Test\MileniumToANS;

use Milenium\Element\Site;
use MileniumToANS\Config;
use MileniumToANS\Exception\MissingConfigFileException;
use MileniumToANS\Exception\MissingSourceFileException;
use MileniumToANS\Exception\WrongConfigFormatException;
use MileniumToANS\Exception\WrongXmlFormatException;
use MileniumToANS\FilesystemInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

/**
 * Class ConfigTest
 *
 * @package MileniumToANS
 */
class ConfigTest extends TestCase
{

    /**
     * @var FilesystemInterface|MockObject $filesystem
     */
    private $filesystem;

    /**
     * @var Config $config
     */
    private $config;

    protected function setUp(): void
    {
        $this->filesystem = $this->getMockBuilder(FilesystemInterface::class)
            ->getMock();

        $this->config = new Config(
            $this->filesystem
        );
    }

    public function testConfigNullSource()
    {
        $this->assertEquals([], $this->config->config());
    }

    public function testConfigMissingConfigFile()
    {
        $source = 'missing-config-file';

        $this->expectException(MissingConfigFileException::class);

        $this->filesystem->expects($this->once())
            ->method('fileExists')
            ->with($source)
            ->willReturn(false);

        $this->config->config($source);
    }

    public function testConfigFileIsNotJson()
    {
        $source = 'config-file';

        $this->filesystem->expects($this->once())
            ->method('fileExists')
            ->with($source)
            ->willReturn(true);

        $this->filesystem->expects($this->once())
            ->method('readFileContents')
            ->with($source)
            ->willReturn('wrong format');

        $this->expectException(WrongConfigFormatException::class);

        $this->config->config('config-file');
    }

    public function testConfigFileIsOk()
    {
        $source = 'config-file';
        $response = ['foo' => 'bar'];

        $this->filesystem->expects($this->once())
            ->method('fileExists')
            ->with($source)
            ->willReturn(true);

        $this->filesystem->expects($this->once())
            ->method('readFileContents')
            ->with($source)
            ->willReturn(json_encode($response));

        $this->assertEquals($response, $this->config->config('config-file'));
    }

}

<?php

namespace Test\MileniumToANS;

use ANSSchema\Story;
use Milenium\Element\Site;
use MileniumToANS\Config;
use MileniumToANS\Exception\MissingConfigFileException;
use MileniumToANS\Exception\MissingDestinationFolderException;
use MileniumToANS\Exception\MissingSourceFileException;
use MileniumToANS\Exception\WrongConfigFormatException;
use MileniumToANS\Exception\WrongXmlFormatException;
use MileniumToANS\FilesystemInterface;
use MileniumToANS\Saver;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Swaggest\JsonSchema\Exception\TypeException;

/**
 * Class SaverTest
 *
 * @package MileniumToANS
 */
class SaverTest extends TestCase
{

    /**
     * @var FilesystemInterface|MockObject $filesystem
     */
    private $filesystem;

    /**
     * @var Saver $saver
     */
    private $saver;

    protected function setUp(): void
    {
        $this->filesystem = $this->getMockBuilder(FilesystemInterface::class)
            ->getMock();

        $this->saver = new Saver(
            $this->filesystem
        );
    }

    public function testSaveMissingDestinationFolder()
    {
        $destination = 'missing-destination-folder';
        $articles = [];

        $this->expectException(MissingDestinationFolderException::class);

        $this->filesystem->expects($this->once())
            ->method('createDirectory')
            ->with($destination)
            ->willReturn(false);

        $this->saver->save($destination, $articles);
    }

    public function testSaveWrongArticleFormat()
    {
        $destination = 'destination-folder';
        $articles = ['wrong-format' => true];

        $this->expectException(TypeException::class);

        $this->filesystem->expects($this->once())
            ->method('createDirectory')
            ->with($destination)
            ->willReturn(true);

        $this->saver->save($destination, $articles);
    }

    public function testSaveOk()
    {
        $destination = 'destination-folder';
        $story = (new Story())
            ->setType(Story::STORY)
            ->setVersion(Story::CONST_0_10_4);
        $articles = [$story];

        $this->filesystem->expects($this->once())
            ->method('createDirectory')
            ->with($destination)
            ->willReturn(true);

        $this->filesystem->expects($this->once())
            ->method('createFile')
            ->with($destination . '/0.json', '{"type":"story","version":"0.10.4"}')
            ->willReturn(true);

        $this->saver->save($destination, $articles);
    }

}
